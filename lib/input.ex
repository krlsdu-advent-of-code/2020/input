defmodule Input do
  @moduledoc """
  Documentation for `Input`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Input.year2020([1824,196,1683,1606, 196])
      357504

  """
  def year2020(numbers) do
    parcelas = Enum.find_value(numbers, fn x -> if Enum.any?(numbers, fn y -> (2020 - x) == y end), do: [x, 2020-x] end)
    (List.last parcelas) * (List.first parcelas)
  end
end
